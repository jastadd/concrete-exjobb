import beaver.Symbol;

%%

%class Scanner
%public
%extends beaver.Scanner
%{
StringBuffer strbuf = new StringBuffer();

private Symbol sym(short id) {
    return new Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }

  private Symbol sym(short id, Object value) {
    return new Symbol(id, yyline + 1, yycolumn + 1, yylength(), value);
  }

  private void error(String msg) throws beaver.Scanner.Exception {
    throw new beaver.Scanner.Exception("(" + (yyline + 1) + ":" + (yycolumn +
    1) + ") " + msg);
  }
%}
%unicode
%line
%column
%function nextToken
%yylexthrow beaver.Scanner.Exception
%type Symbol
%eofval{
  return sym(Parser.Terminals.EOF, "end-of-file");
%eofval}

LineTerminator = \r | \n | \r\n
CommentCharacter = [^\r\n\\\"]
WhiteSpace     = {LineTerminator} | [ \t\f]

Comment = {TraditionalComment}
        | {EndOfLineComment}

TraditionalComment = "/*" [^*] ~"*/" | "/*" "*"+ "/" | "/*" "*"+ [^/*] ~"*/"
EndOfLineComment = "//" {CommentCharacter}* {LineTerminator}?

//patterns from tokenrules are placed here
//followed by symbol creation rules (from tokenrules and literal strings in productions)
