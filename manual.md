The Concrete grammar language for JastAdd
=========================================

Concrete is a concrete grammar specification language for JastAdd. It is backwards compatible with JastAdd's abstract grammar specification language and adds support for concrete syntax and some functionality for working with common ambiguous grammars.

Key features
------------
* Specification of abstract and concrete syntax in one single place
* Easy specification of grammars that are often ambiguous, using associativity and precedence
* Translation from Concrete to JastAdd's abstract grammar language, JFlex and Beaver

Running the Concrete compiler
-----------------------------
To translate a (number of) specification file(s) into AST, JFlex and Beaver specifications:

    java -jar concrete-compiler.jar <options> <input files>

Options (optional):

* `--o` Output directory, default is current
* `--package` Package to place JastAdd-generated files under, default is none
* `--jflexpre` Starting JFlex boilerplate specification, default is `concrete-compiler.jar/jflextemplates/JFlexPrelude.flex`
* `--jflexpost` Ending JFlex boilerplate specification, default is `concrete-compiler.jar/jflextemplates/JFlexPostlude.flex`
* `--beaverpre` Beaver boilerplate specification
* `--argfile` File containing options and arguments, one on each row. When this option is in use, all other options and input files are ignored.
* `--help` Prints a description of the options

Input files can be a mix of `.conc` files and other files. Only `.conc` files will be processed.

To run Concrete followed by JastAdd, JFlex, JastAddParser and Beaver (this will produce Java files ready for compilation):

    ./concrete <options> <input files>

Run the script with the option `-h` to get a description of the other options. Input files can be a mix of `.conc` files and other files. `.conc` files will be sent to the Concrete compiler, other files will be sent to JastAdd together with the generated AST specification.

Grammar structure
-----------------
A Concrete grammar consists of a number of *rules* that describe different parts of the language. The rules are divided into *sections*. The most important sections are *parse rules*, that specify parser productions and declare AST node types, *class rules*, that declare node types that are created outside the parsing process, and *token rules*, that specify regular expressions for elements such as identifiers and numeric literals.

The order of the sections is not important, and any number of sections of the same type may be present in a specification. There is also limited support for modular grammars: the Concrete compiler can process several files that together compose a complete grammar. However in some cases, the order in which token rules are processed make a difference.

Parse rules
-----------
The parse rules section is the most central part of the grammar. It starts with the heading `parserules` and describes AST node types and their relations; what kinds of children a node will have and how the Java class hierarchy is composed. A parse rule contains, similarly to a context-free grammar production, a non-terminal symbol on the left-hand side and an optional production on the right-hand side. The left-hand side is the name of the generated node type (Java class). A parse rule is specified according to the following format:

    ["abstract"] Name [":" SuperType] ["::=" ElementList] ";"

The left-hand side may specify a super type of the declared type, as in the AST language. This must refer to an existing node type and both names must be valid Java identifiers. `ElementList` represents a sequence of symbols that the parser will use to construct a node of the declared type. These symbols can have the following formats:


* **`RuleName`** This refers to a parse rule in the grammar and will be represented as a child node of the type `RuleName`. The child will have the name `RuleName`.
* **`ChildName:RuleName`** As above, but the child will have the type `RuleName` and the name `ChildName`.
* **`"keyword"`** The string `keyword`. This element will not be reflected in the AST.
* **`<TOKEN>`** A string according to the format specified by the token rule `TOKEN`. This element will not be reflected in the AST.
* **`<NAME:TOKEN>`** As above, but the actual matched string will be saved in the AST as a field on the declared node type. The field will have the name `NAME`, which must be a unique name among the fields of this node type, and the Java type `String`. The name after the colon must refer to a specified token rule.
* **`RuleName*`** This will accept several nodes of the same type and store them in a child node of the type `List`. The `List` type is predefined by JastAdd. The child may be named like any other child.
* **`RuleName+`** As above, but the list may not be empty (i.e. at least one symbol is expected).

In addition to these formats, three types of so-called *element groups* are also allowed:

* **Optional group** An optional group consists of zero or one non-terminal symbols and any number of string literals or unnamed tokens enclosed in square brackets. If there is no non-terminal symbol in the group, it will not be reflected in the AST. Otherwise, it will be represented as a child of the type `Opt` that may or may not have a child depending on whether the non-terminal symbol was matched. The `Opt` type is predefined by JastAdd. Example:

        SimpleClassDecl ::= "class" IdDecl ["extends" IdUse] Block;

* **List group** A list group consists of exactly one non-terminal symbol and any number of string literals or unnamed tokens enclosed in parentheses, followed by a Kleene star (`*`) or a plus sign (`+`) depending on the type of list. A list group corresponds to a `List` node in the AST, just like the stand-alone list element described earlier. The list will contain the matched non-terminal symbols; the string literals and tokens are not saved in the AST. Example:

         StmtList ::= (Stmt ";")*;

* **List group with separator** A list group may specify `sep=separator` at the end, where `separator` is either a string literal or an unnamed token. This allows for simple parsing of lists that are separated by some symbol, where the separator is not repeated after the last element. Example:

         VoidMethodDecl ::= "void" IdDecl "(" (ParamDecl sep=",")* ")" Block;

* **Optional list** This is essentially an optional group that contains a list element or a list group instead of a single non-terminal symbol. The difference is that it will result in a `List` child instead of an `Opt`. If no symbols in the group are matched in the input, the list is empty. Example:

         ClassDecl ::= "class" IdDecl ["extends" IdUse] ["implements" (IdUse sep=",")*] Block;

If a rule has the keyword `abstract`, the generated node type will be abstract. This means that it cannot be instantiated; not by a parser or from anywhere else. Consequently, anonymous tokens (i.e. string literals and unnamed tokens) are not allowed in the element lists of such rules. This restriction implies that element groups are only allowed in abstract rules if they contain only one element, and this element is not a string literal or an unnamed token.

A rule that specifies a super type must reiterate every child and named token from its direct and indirect super types. The inherited elements may be listed in a different order than its super type, if that is necessary for the desired syntax. Anonymous tokens do not have any restrictions and may be replaced, removed or repeated arbitrarily between inheriting types. Thus, non-abstract sub types may "override" an element group from a super type by changing the syntactical elements in it:

    abstract A ::= B*;
    C: A ::= (B ";")*;
    D: A ::= (B sep=",")*;

Also notice that abstract parse types must have a concrete sub type somewhere in the inheritance chain.

Class rules
-----------

The class rules section heading is `classrules` and the rules specified here are syntactically identical and functionally equivalent to the declarations in the AST language. In order to preserve backwards compatibility with existing grammar specifications, the class rules section heading is considered implicit at the beginning of a file.

The node classes in this type of section can not be instantiated during parsing. Such nodes are typically used during post-parsing tree transformations. A class rule has the same general format as a parse rule:

    ["abstract"] Name [":" SuperType] ["::=" ProductionList] ";"

The difference lies in what types of symbols are allowed in the production list:

* **`RuleName`** This and its named variant is similar to its parse rule counterpart, but `RuleName` may refer to another class rule as well as a parse rule.
* **`<Name:Token>`** This corresponds to a field with the Java type `Token`, saved on the resulting node. The field will have the name `Name`. Notice that this has a different meaning than the corresponding parse rule syntax. The reason is to preserve backwards compatibility with JastAdd's abstract grammar language, which supports fields of types other than `String`.
* **`<Token>`** This corresponds to an arbitrary string that will be saved as a field with the name `Token` on the resulting node. Note that this is also different from its parse rule counterpart: here it is short for `<Token:String>`, since the name after the colon refers to a Java type and not a "token type".
* **`RuleName*`** This and its named variant is similar to its parse rule counterpart in that it creates a list of child nodes, but here `RuleName` may refer to another class rule as well as a parse rule. Class rules do not allow list groups, and it is not possible to specify non-empty lists.
* **`[RuleName]`** An optional child. Like the optional groups that parse rules can specify, this results in a child of the type `Opt` that in turn may or may not have a child.

Token rules
-----------
The token rules section is preceded by the heading `tokenrules` and specifies patterns that match special terminal symbols in the grammar. Many terminal symbols do not need to be declared with a token rule: keywords, operators and similar symbols that only have a single possible value can and should be specified directly in the productions where they are used. Other terminals may match several strings and need to be defined using a regular expression. This is where token rules come into play. A common example is an identifier, which can have different values that are parsed as the same terminal symbol. Its value typically needs to be accessible in the syntax tree. Another example is a comment, which typically is not stored in the tree. A token rule has the following format:

    "<" NAME ">" "=" REGEX ";"

`NAME` is the name of the pattern and must be a valid Java identifier. `REGEX` is a string containing the regular expression that describes the pattern. If a semicolon is used in the string, it needs to be escaped using a backslash: `\;`.

Priorities
----------
A priorities section is headed by `priorities` and consists of statements that describe how conflicting parse rules may be disambiguated. The statements may take two forms:

    A > B;

or

    A == B;

The statements have their intuitive meaning: The first form denotes that the parse rule `A` should be given higher priority than the parse rule `B`, effectively making the elements of production `A` "bind tighter" than `B`. The second form means that the two productions have equal priority. It is possible to specify "chains" of priorities on a single line, as in

    A > B > C;
    C == D == E;

The priority relation is computed transitively when possible, so that the specification can be as small as possible if desired. The rules that are mentioned in either a priority or associativity statement need to have a strict total order of priority defined, so that there is no ambiguity in the priority relation. For example, if the following priority section is specified, an error message will be displayed since the relationship between `B` and `C` is not defined:

    priorities
    A > B;
    A > C;

As a real example, the following section specifies the usual precedence of the four basic arithmetic operations and exponentiation:

    priorities
    Multiplication == Division;
    Addition == Subtraction;
    Exponentiation > Multiplication;
    Multiplication > Addition;

Associativity
-------------

There are three types of associativity sections; `left-associative`, `right-associative` and `non-associative`. They consist of statements containing only a name of a parse rule, like so:

    A;

The meaning of each statement is, naturally, that the referenced production has the associativity corresponding to the section it is placed in. If a production is mentioned in a priority statement but not in any associativity statement, it will be implicitly considered left-associative if applicable. (Notice that a production is not allowed to be present in an associativity section without also being in a priority section.) An associativity specification of the basic arithmetic operations and exponentiation would be:

    left-associative
    Multiplication;
    Division;
    Addition;
    Subtraction;
    right-associative
    Exponentiation;

The `left-associative` section above may be left out due to the default behaviour, as long as the corresponding priority relation of the productions make a strict total order. The Beaver back end requires that the rule definition contains at least one terminal symbol.

