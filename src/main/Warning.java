package main;

public class Warning extends Problem {

	public Warning(int lineStart, int colStart, int lineEnd, int colEnd,
			String message) {
		super(lineStart, colStart, lineEnd, colEnd, message);
	}
	
	public Warning(String message) {
		super(message);
	}
	
	public boolean isError() {
		return false;
	}

}
