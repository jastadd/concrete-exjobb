package main;

public class Error extends Problem {

	public Error(int lineStart, int colStart, int lineEnd, int colEnd,
			String message) {
		super(lineStart, colStart, lineEnd, colEnd, message);
	}
	
	public Error(String message) {
		super(message);
	}
	
	public boolean isError() {
		return true;
	}

}
