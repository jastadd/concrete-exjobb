package main;

/**
 * Concrete grammar problem.
 */
public abstract class Problem implements Comparable<Problem> {

	private final String message;
	private final int lineStart;
	private final int colStart;
	private final int lineEnd;
	private final int colEnd;

	/**
	 * @param message
	 * @param file
	 * @param line
	 * @param column
	 */
	public Problem(int lineStart, int colStart, int lineEnd, int colEnd,
			String message) {
		this.lineStart = lineStart;
		this.colStart = colStart;
		this.lineEnd = lineEnd;
		this.colEnd = colEnd;
		this.message = message;
	}
	
	public Problem(String message) {
		this(-1, -1, -1, -1, message);
	}

	/**
	 * @return <code>true</code> if this is an error, <code>false</code> if it
	 *         is a warning
	 */
	public abstract boolean isError();

	@Override
	public String toString() {
		String kind = isError() ? "Error" : "Warning";
		String loc = "";
		if (lineStart != -1) {
			loc = "(" + lineStart + ":" + colStart + "-" + lineEnd + ":"
					+ colEnd + ")";
		}
		return kind + loc + ": " + message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public int compareTo(Problem o) {
		int diff = lineStart - o.lineStart;
		if (diff == 0) {
			return colStart - o.colStart;
		}
		return diff;
	}

}
