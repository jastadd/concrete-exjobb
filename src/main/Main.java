package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import parser.Parser;
import parser.Scanner;
import ast.Grammar;

/**
 * Concrete main class. Concatenates Concrete specifications and outputs error
 * messages if applicable. If no errors, generates specifications for JastAdd
 * abstract grammar, JFlex and Beaver.
 * 
 * @author Harald Görtz <harald.gortz@gmail.com>
 * 
 */
public class Main {

	/**
	 * @param args
	 *            Options followed by file paths. At least one conc file must be
	 *            specified. Files not ending with .conc will be ignored.
	 */
	public static void main(String[] args) {
		String outDir = null;
		String pkg = null;
		String jFlexPre = null;
		String jFlexPost = null;
		String beaverPre = null;
		int i = 0;
		for (i = 0; i < args.length; i++) {
			if (args[i].equals("--o")) {
				outDir = args[++i];
			} else if (args[i].equals("--package")) {
				pkg = args[++i];
			} else if (args[i].equals("--jflexpre")) {
				jFlexPre = args[++i];
			} else if (args[i].equals("--jflexpost")) {
				jFlexPost = args[++i];
			} else if (args[i].equals("--beaverpre")) {
				beaverPre = args[++i];
			} else if (args[i].equals("--argfile")) {
				outDir = null;
				pkg = null;
				jFlexPre = null;
				jFlexPost = null;
				beaverPre = null;
				args = readArguments(args[++i]);
				i = -1;
			} else if (args[i].equals("--help")) {
				printHelp();
				System.exit(0);
			} else {
				break;
			}
		}

		File concatFile = new File(outDir, "concat.conc");
		FileWriter fw = null;
		java.util.Scanner scan = null;
		boolean foundInputFiles = false;
		try {
			fw = new FileWriter(concatFile);
			for (; i < args.length; i++) {
				if (args[i].endsWith(".conc")) {
					foundInputFiles = true;
					scan = new java.util.Scanner(new FileReader(new File(args[i])));
					scan.useDelimiter("\\Z");
					fw.append("classrules\n");
					fw.append(scan.next());
					scan.close();
				}
			}
		} catch (IOException e1) {
			System.err.println("Could not open " + e1.getMessage());
			System.exit(1);
		} finally {
			try {
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		if (!foundInputFiles) {
			System.err.println("No conc files specified. No output provided.");
			System.exit(1);
		}

		Scanner scanner = null;
		Parser parser = null;
		try {
			scanner = new Scanner(new FileReader(concatFile.getAbsolutePath()));
			parser = new Parser();
		} catch (FileNotFoundException e) {
			System.err.println("Generated file not found: " + concatFile);
			System.exit(1);
		}
		Grammar g = null;
		try {
			g = (Grammar) parser.parse(scanner);
		} catch (IOException e) {
			System.err.println("Could not read generated conc specification "
					+ concatFile.getAbsolutePath());
			System.exit(1);
		} catch (beaver.Parser.Exception e) {
			System.err.println("Syntax error in conc specifications");
			System.exit(1);
		}
		if (!g.errors().isEmpty()) {
			System.err.println("Error(s) in grammar:");
			for (Problem p : g.errors()) {
				System.err.println(p.toString());
			}
			System.exit(1);
		}
		for (Problem p : g.warnings()) {
			System.out.println(p.toString());
		}

		Properties props = new Properties();
		if (pkg != null) {
			props.put("package", pkg);
		}
		if (jFlexPre != null) {
			props.put("jflexpre", jFlexPre);
		}
		if (jFlexPost != null) {
			props.put("jflexpost", jFlexPost);
		}
		if (beaverPre != null) {
			props.put("beaverpre", beaverPre);
		}
		g.setOptions(props);

		FileWriter out;
		try {
			out = new FileWriter(new File(outDir, "AST.ast"));
			g.dumpAstSpec(out);
			out.close();
			out = new FileWriter(new File(outDir, "Parser.parser"));
			g.writeBeaverSpec(out);
			out.close();
			out = new FileWriter(new File(outDir, "Scanner.flex"));
			g.writeJFlexSpec(out);
			out.close();
		} catch (IOException e) {
			System.err.println("Could not write generated specification files: "
							+ e);
		}
	}

	private static String[] readArguments(String argFilename) {
		java.util.Scanner scan;
		List<String> args = null;
		try {
			scan = new java.util.Scanner(new File(argFilename));
			args = new ArrayList<String>();
			while (scan.hasNextLine()) {
				String line = scan.nextLine().trim();
				int splitIndex = line.indexOf(' ');
				if (splitIndex > 0 && line.startsWith("--")) {
					args.add(line.substring(0, splitIndex));
					args.add(line.substring(splitIndex + 1));
				} else {
					args.add(line);
				}
			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.err.println("Argument file not found: " + argFilename);
			System.exit(1);
		}
		return args.toArray(new String[args.size()]);
	}

	private static void printHelp() {
		System.out.println("Concrete compiler");
		System.out.println();
		System.out.println("Options:");
		System.out.println("--o         base output directory (default: current)");
		System.out.println("--package   package for generated AST files (default: none)");
		System.out.println("--jflexpre  leading JFlex boilerplate specification");
		System.out.println("--jflexpost trailing JFlex boilerplate specification");
		System.out.println("--beaverpre Beaver boilerplate specification");
		System.out.println("--argfile   file containing one option/argument on each row");
		System.out.println("            (will ignore all other command line opts/args)");
		System.out.println("--help      this text");
	}
}
