package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import main.Problem;
import parser.Parser;
import parser.Scanner;
import ast.Grammar;

public class TestRunner {

	public static void main(String[] args) {
		String testName = "picojava";
		runTest("test/", testName, "tmp/");
		System.out.println(testName + ": Success!");
	}
	
	private static final String SYS_FILE_SEP = System.getProperty("file.separator");
	private static final String SYS_PATH_SEP = System.getProperty("path.separator");
	private static final String SYS_LINE_SEP = System.getProperty("line.separator");
	
	/** Test property key for expected test result */
	public static final String TEST_RESULT = "result";

	/** Test property key for arguments to pass to Concrete */
	public static final String CONCRETE_ARGS = "concreteArgs";

	/** Test property key for arguments to pass to the test program */
	public static final String TEST_ARGS = "testArgs";

	/**
	 * The grammar is parsed without problems and the test program completes
	 * normally
	 */
	public static final String RESULT_PASS = "pass";

	/** Errors are found while parsing the grammar and specific output is given */
	public static final String RESULT_ERRORS = "error";

	/**
	 * Warnings are issued while parsing the grammar and specific output is
	 * given
	 */
	public static final String RESULT_WARNINGS = "warning";

	/**
	 * The grammar is parsed without problems and the test program completes
	 * normally with specific output
	 */
	public static final String RESULT_OUTPUT_PASS = "output";

	/**
	 * The grammar is parsed without problems and the test program terminates
	 * with specific error output
	 */
	public static final String RESULT_OUTPUT_ERRORS = "output_errors";

	public static void runTest(String testRoot, String testName, String tmpRoot) {
		String testPath = testRoot + testName + SYS_FILE_SEP;
		String tmpPath = tmpRoot + testName + SYS_FILE_SEP;
		Properties props = getProperties(testPath);
		String result = getTestResult(props);
		File concFile = findFileWithSuffix(new File(testPath), "conc");
		
		String fileName = concFile.getName().substring(0, concFile.getName().length() - 4);
		File tmpDir = new File(tmpPath);
		if (!tmpDir.exists()) {
			tmpDir.mkdirs();
		} else {
			cleanDirectory(tmpDir);
		}
		Scanner scanner = null;
		Parser parser = null;
		try {
			scanner = new Scanner(new FileReader(concFile.getAbsolutePath()));
			parser = new Parser();
		} catch (FileNotFoundException e) {
			fail("Could not open conc specification " + concFile.getAbsolutePath());
		}
		Grammar g = null;
		try {
			g = (Grammar) parser.parse(scanner);
		} catch (IOException e) {
			fail("Could not read conc specification " + concFile.getAbsolutePath());
		} catch (beaver.Parser.Exception e) {
			fail("Syntax error in conc specification " + concFile.getAbsolutePath());
		}
		setConcreteOptions(props.getProperty(CONCRETE_ARGS), g);
		
		if (g.warnings().isEmpty() && result.equals(RESULT_WARNINGS)) {
			fail("Expected warnings when parsing grammar but none were issued");
		} else if (!g.warnings().isEmpty() && result.equals(RESULT_WARNINGS)) {
			compareErrors(g.warnings(), testPath);
			return;
		}
		
		if (g.errors().isEmpty()) {
			if (result.equals(RESULT_ERRORS)) {
				fail("Test succeeded when expected to fail");
			}
			FileWriter out;
			try {
				out = new FileWriter(new File(tmpPath, fileName + "ast"));
				g.dumpAstSpec(out);
				out.close();
				out = new FileWriter(new File(tmpPath, fileName + "parser"));
				g.writeBeaverSpec(out);
				out.close();
				out = new FileWriter(new File(tmpPath, fileName + "flex"));
				g.writeJFlexSpec(out);
				out.close();
			} catch (IOException e) {
				fail("Could not write generated specification files to " + tmpPath + ": " + stackTraceAsString(e));
			} catch (Exception e) {
				fail("Runtime error from generated files: " + stackTraceAsString(e));
			}
		} else {
			if (result.equals(RESULT_ERRORS)) {
				compareErrors(g.errors(), testPath);
				return;
			} else {
				StringBuilder sb = new StringBuilder();
				sb.append(concFile.getPath()).append(": Error in conc specification: ").append(SYS_LINE_SEP);
				for (Problem p : g.errors()) {
					sb.append(p).append(SYS_LINE_SEP);
				}
				fail(sb.toString());
			}
		}
		
		Process p = null;
		try {
			p = Runtime.getRuntime().exec("java -jar tools/JastAddParser.jar "
					+ tmpPath + fileName + "parser "
					+ tmpPath + "Parser.beaver");
			int exitValue = p.waitFor();
			printErrorsAndExit(p.getErrorStream(), exitValue);
			
			p = Runtime.getRuntime().exec("java -jar tools/beaver-cc.jar " + "-d " + tmpPath + ' '
					+ tmpPath + "Parser.beaver");
			exitValue = p.waitFor();
			printErrorsAndExit(p.getErrorStream(), exitValue);
			
			p = Runtime.getRuntime().exec("java -jar tools/JFlex.jar -q -d " + tmpPath + ' '
					+ tmpPath + fileName + "flex");
			exitValue = p.waitFor();
			printErrorsAndExit(p.getInputStream(), p.getErrorStream(), exitValue);
			
			String packageString = g.getPackageName();
			packageString = packageString == null ? "" : "--package=" + packageString;
			StringBuilder sb = new StringBuilder("java -jar tools/jastadd2.jar --o " + tmpPath
					+ ' ' + packageString + " --rewrite --beaver " + tmpPath + fileName + "ast ");
			for (String s : collectFilesWithSuffix(testPath, "jrag", true)) {
				sb.append(s).append(' ');
			}
			for (String s : collectFilesWithSuffix(testPath, "jadd", true)) {
				sb.append(s).append(' ');
			}
			p = Runtime.getRuntime().exec(sb.toString());
			exitValue = p.waitFor();
			printErrorsAndExit(p.getErrorStream(), exitValue);
		
		} catch (IOException e) {
			fail("Could not fork process: " + stackTraceAsString(e));
		} catch (InterruptedException e) {
			fail("Process interrupted: " + stackTraceAsString(e));
		}
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		if (compiler == null) {
			fail("No Java compiler found");
		}
		ByteArrayInputStream in = new ByteArrayInputStream(new byte[0]);
		ByteArrayOutputStream err = new ByteArrayOutputStream();
		ByteArrayOutputStream compilerOut = new ByteArrayOutputStream();
		StringBuilder arguments = new StringBuilder("-d " + tmpPath + " -g -cp tools/beaver-cc.jar");
		arguments.append(SYS_PATH_SEP).append(tmpPath);
		List<String> sourceFiles = collectFilesWithSuffix(tmpPath, "java", true);
		sourceFiles.addAll(collectFilesWithSuffix(testPath, "java", true));
		for (String file : sourceFiles) {
			arguments.append(' ').append(file);
		}
		
		int exitValue = compiler.run(in, compilerOut, err, arguments.toString().split(" "));
		if (exitValue != 0) {
			fail(compilerOut.toString() + SYS_LINE_SEP + err.toString());
		}
		try {
			String testArgs = props.getProperty(TEST_ARGS);
			p = Runtime.getRuntime().exec("java -cp tools/beaver-cc.jar" + SYS_PATH_SEP + tmpPath + " Test " + testArgs);
			exitValue = p.waitFor();
		} catch (IOException e) {
			fail("Could not fork process: " + stackTraceAsString(e));
		} catch (InterruptedException e) {
			fail("Process interrupted: " + stackTraceAsString(e));
		}
		if (result.equals(RESULT_OUTPUT_ERRORS)) {
			compareOutput(p.getErrorStream(), testPath);
		} else {
			printErrorsAndExit(p.getErrorStream(), exitValue);
		}
		if (result.equals(RESULT_OUTPUT_PASS)) {
			compareOutput(p.getInputStream(), testPath);
		}
	}

	private static String getTestResult(Properties props) {
		String r = (String) props.get(TEST_RESULT);
		if (r.equals(RESULT_ERRORS) || r.equals(RESULT_OUTPUT_PASS)
				|| r.equals(RESULT_PASS) || r.equals(RESULT_WARNINGS)
				|| r.equals(RESULT_OUTPUT_ERRORS)) {
			return r;
		} else {
			fail("Invalid test result: " + r);
		}
		return null;
	}

	private static void setConcreteOptions(String options, Grammar g) {
		Properties defaults = new Properties();
		defaults.put("package", "ast");
		Properties props = new Properties(defaults);
		try {
			props.load(new java.io.StringReader(options));
		} catch (IOException e) {
			fail("I/O error: " + stackTraceAsString(e));
		}
		g.setOptions(props);
	}

	private static void printErrorsAndExit(InputStream err, int exitValue) {
		printErrorsAndExit(null, err, exitValue);
	}
	private static void printErrorsAndExit(InputStream out, InputStream err, int exitValue) {
		if (exitValue != 0) {
			java.util.Scanner scan = new java.util.Scanner(err);
			StringBuffer errors = new StringBuffer();
			if (scan.hasNextLine()) {
				while (scan.hasNextLine()) {
					errors.append(scan.nextLine()).append(SYS_LINE_SEP);
				}
			} else if (out != null) {
				scan.close();
				scan = new java.util.Scanner(out);
				while (scan.hasNextLine()) {
					errors.append(scan.nextLine()).append(SYS_LINE_SEP);
				}
			}
			scan.close();
			fail(errors.toString());
		}
	}
	
	private static void cleanDirectory(File testDir) {
		for (File file : testDir.listFiles()) {
			if (file.isFile())
				file.delete();
			else
				cleanDirectory(file);
		}
	}
	
	private static File findFileWithSuffix(File dir, String suffix) {
		for (File f : dir.listFiles()) {
			if (f.getName().endsWith(suffix)) {
				return f;
			}
		}
		return null;
	}
	
	/*
	 * Collect file names ending with a specific suffix from the specified
	 * directory
	 *
	 * @param dirPath
	 *            the directory to search
	 * @param suffix
	 *            the suffix to match
	 * @param recursive
	 *            if set to true, the whole directory sub-tree will be searched
	 * @return a java.util.List of the complete file names relative to the
	 *         system root directory
	 */
	private static List<String> collectFilesWithSuffix(String dirPath,
			String suffix, boolean recursive) {
		ArrayList<String> ans = new ArrayList<String>();
		File dir = new File(dirPath);

		for (File f : dir.listFiles()) {
			if (f.isFile() && f.getName().endsWith(suffix)) {
				ans.add(f.getPath());
			} else if (recursive && f.isDirectory()) {
				ans.addAll(collectFilesWithSuffix(f.getPath(), suffix,
						recursive));
			}
		}

		return ans;
	}
	
	private static void compareErrors(Collection<Problem> problems, String testPath) {
		List<String> expectedMsgLines = new ArrayList<String>();
		List<String> expectedMsgLines2 = new ArrayList<String>();
		try {
			java.util.Scanner scan = new java.util.Scanner(new File(testPath, "testoutput"));
			while (scan.hasNextLine()) {
				String s = scan.nextLine();
				expectedMsgLines.add(s);
				expectedMsgLines2.add(s);
			}
			scan.close();
		} catch (FileNotFoundException e) {
			fail("File testoutput not found");
		}

		int expectedLineCount = expectedMsgLines.size();
		int matchedLineCount = 0;
		int actualLineCount = 0;
		for (Problem p : problems) {
			String[] msgLines = p.getMessage().split("\n");
			for (String s : msgLines) {
				actualLineCount++;
				if (expectedMsgLines.remove(s)) {
					matchedLineCount++;
				}
			}
		}
		if (actualLineCount != expectedLineCount || matchedLineCount < expectedLineCount) {
			StringBuilder error = new StringBuilder("Output mismatch. Expected:" + SYS_LINE_SEP);
			for (String s : expectedMsgLines2) {
				error.append(s).append(SYS_LINE_SEP);
			}
			error.append("Actual:").append(SYS_LINE_SEP);
			for (Problem p : problems) {
				error.append(p.getMessage()).append(SYS_LINE_SEP);
			}
			fail(error.toString());
		}
	}
	
	private static void compareOutput(InputStream in, String testPath) {
		String expected = null;
		String actual = null;
		try {
			java.util.Scanner scan = new java.util.Scanner(new File(testPath, "testoutput"));
			scan.useDelimiter("\\Z");
			expected = scan.hasNext() ? scan.next() : "";
			scan.close();
			scan = new java.util.Scanner(in);
			scan.useDelimiter("\\Z");
			actual = scan.hasNext() ? scan.next() : "";
			scan.close();
		} catch (FileNotFoundException e) {
			fail("File testoutput not found");
		}
		assertEquals("Output mismatch.", expected, actual);
	}
	
	private static String stackTraceAsString(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return SYS_LINE_SEP + sw.toString();
	}
	
	private static Properties getProperties(String testPath) {
		Properties defaults = new Properties();
		defaults.put("result", "pass");
		defaults.put("concreteArgs", "");
		defaults.put("testArgs", testPath);
		Properties props = new Properties(defaults);
		try {
			FileInputStream in = new FileInputStream(new File(testPath, "test.properties"));
			props.load(in);
			in.close();
		} catch (IOException e) {
			fail("Could not read properties file in " + testPath);
		}
		return props;
	}
	
	public static Collection<String> getTests(File currentDir, File rootDir) {
		List<String> tests = new ArrayList<String>();
		for (File f : currentDir.listFiles()) {
			if (f.isDirectory()) {
				tests.addAll(getTests(f, rootDir));
			} else if (f.getName().endsWith(".conc")) {
				String rootPath = rootDir.getPath();
				String testPath = currentDir.getPath();
				if (testPath.startsWith(rootPath + SYS_FILE_SEP)) {
					testPath = testPath.substring(rootPath.length() + 1);
				}
				tests.add(testPath);
			}
		}
		// sort result
		Collections.sort(tests);
		return tests;
	}

}
