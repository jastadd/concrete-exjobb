aspect AssocErrorMessages {
	PrioStmt contributes error(getLeft().getID() + " is not defined")
	when leftRule() == null
	to Grammar.semanticErrors() for grammar();
	
	PrioStmt contributes error(getRight().getID() + " is not defined")
	when rightRule() == null
	to Grammar.semanticErrors() for grammar();
	
	PrioStmt contributes error(getLeft().getID() + " is not a non-abstract parse rule")
	when leftRule() != null && !(leftRule() instanceof ConcreteParseRule)
	to Grammar.semanticErrors() for grammar();
	
	PrioStmt contributes error(getRight().getID() + " is not a non-abstract parse rule")
	when rightRule() != null && !(rightRule() instanceof ConcreteParseRule)
	to Grammar.semanticErrors() for grammar();
	
	AssocStmt contributes error(getIdUse().getID() + " is not defined")
	when decl() == null
	to Grammar.semanticErrors() for grammar();
	
	AssocStmt contributes error(getIdUse().getID() + " is not a non-abstract parse rule")
	when decl() != null && !(decl() instanceof ConcreteParseRule)
	to Grammar.semanticErrors() for grammar();

	AssocStmt contributes error("Associativity already defined for " + getIdUse())
	when decl() != null && decl().associativity() != null && decl().associativity() != this
	to Grammar.semanticErrors() for grammar();

}

aspect PriorityCheck {
	
	GtPrioStmt contributes error("Cyclic priority specification on " + leftRule().eqString() + " (" + toString() + ")")
	when hasCyclicPrio()
	to Grammar.semanticErrors() for grammar();
	
	ParseRule contributes error("Ambiguous priority relationship between " + ambiguousPriorityString())
	when ambiguousPrioRules() != null && !hasCyclicPrio()
	to Grammar.semanticErrors() for grammar();
	
	syn boolean GtPrioStmt.hasCyclicPrio() = leftRule().hasCyclicPrio() && rightRule().hasCyclicPrio();
	syn boolean ParseRule.hasCyclicPrio() = grammar().cyclicPrioRules().contains(grammar().getEqSet(this));
	
	syn lazy Set<Set<ParseRule>> Grammar.cyclicPrioRules() {
		double[][] dist = grammar().prioGraphReachabilityMatrix();
		Set<Set<ParseRule>> ans = new HashSet<Set<ParseRule>>();
		for (int i = 0; i < dist.length; i++) {
			if (dist[i][i] != Double.POSITIVE_INFINITY) {
				ans.add(precAssocRules().get(i));
			}
		}
		return ans;
	}
	
	syn String ParseRule.ambiguousPriorityString() {
		StringBuilder sb = new StringBuilder();
		for (Set<ParseRule> s : ambiguousPrioRules()) {
			for (ParseRule r : s) {
				sb.append(r.eqString());
				break;
			}
			sb.append(", ");
		}
		sb.delete(sb.length() - 2, sb.length());
		return sb.toString();
	}
	
	syn java.util.List<Set<ParseRule>> ParseRule.ambiguousPrioRules() {
		Set<java.util.List<Set<ParseRule>>> ambiguousRules = grammar().ambiguousPrioRules();
		Set<ParseRule> eqSet = grammar().getEqSet(this);
		String firstRuleName = null;
		for (ParseRule r : eqSet) {
			if (firstRuleName == null || r.getIdDecl().getID().compareTo(firstRuleName) < 0) {
				firstRuleName = r.getIdDecl().getID();
			}
		}
		if (getIdDecl().getID().equals(firstRuleName)) {
			for (java.util.List<Set<ParseRule>> l : ambiguousRules) {
				if (l.get(0).equals(eqSet)) {
					return l;
				}
			}
		}
		return null;
	}
	
	syn lazy Set<java.util.List<Set<ParseRule>>> Grammar.ambiguousPrioRules() {
		ArrayList<java.util.List<Set<ParseRule>>> ambiguousRules = new ArrayList<java.util.List<Set<ParseRule>>>();
	  	double[][] dist = grammar().prioGraphReachabilityMatrix();
	  	for (int i = 0; i < dist.length + 1; i++) {
	  		ambiguousRules.add(new ArrayList<Set<ParseRule>>());
	  	}
		for (int i = 0; i < dist.length; i++) {
			int count = 0;
			for (int j = 0; j < dist.length; j++) {
				if (dist[i][j] == Double.POSITIVE_INFINITY) {
					count++;
				}
			}
			ambiguousRules.get(count).add(precAssocRules().get(i));
		}
		Set<java.util.List<Set<ParseRule>>> ans = new HashSet<java.util.List<Set<ParseRule>>>();
		for (java.util.List<Set<ParseRule>> l : ambiguousRules) {
			if (l.size() > 1) {
				ans.add(l);
			}
		}
		return ans;
	}

}
