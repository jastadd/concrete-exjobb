package parser;
import beaver.Symbol;

%%

%class Scanner
%public
%extends beaver.Scanner
%{
StringBuffer strbuf = new StringBuffer();

private Symbol sym(short id) {
    return new Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }

  private Symbol sym(short id, Object value) {
    return new Symbol(id, yyline + 1, yycolumn + 1, yylength(), value);
  }

  private void error(String msg) throws beaver.Scanner.Exception {
    throw new beaver.Scanner.Exception("(" + (yyline + 1) + ":" + (yycolumn +
    1) + ") " + msg);
  }
%}
%unicode
%line
%column
%function nextToken
%yylexthrow beaver.Scanner.Exception
%type Symbol
%eofval{
  return sym(Parser.Terminals.EOF, "end-of-file");
%eofval}
%state STRING, SEP, REGEX

LineTerminator = \r | \n | \r\n
StringCharacter = [^\r\n\\\"]
RegexCharacter = [^\r\n\\;]
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

Comment = {TraditionalComment}
        | {EndOfLineComment}

TraditionalComment = "/*" [^*] ~"*/" | "/*" "*"+ "/" | "/*" "*"+ [^/*] ~"*/"
EndOfLineComment = "//" {InputCharacter}* {LineTerminator}?

Identifier = [:jletter:] [:jletterdigit:]*

AnyChar	       = . | \R

%%

<YYINITIAL> {
{WhiteSpace}	{}
{Comment}	{}
"parserules" { return sym(Parser.Terminals.PARSERULES); }
"classrules" { return sym(Parser.Terminals.CLASSRULES); }
"tokenrules" { return sym(Parser.Terminals.TOKENRULES); }
"abstract"	{ return sym(Parser.Terminals.ABSTRACT); }
"::="		{ return sym(Parser.Terminals.IS); }
"*"		{ return sym(Parser.Terminals.STAR); }
"+"		{ return sym(Parser.Terminals.PLUS); }
"<"		{ return sym(Parser.Terminals.LT); }
">"		{ return sym(Parser.Terminals.GT); }
"["		{ return sym(Parser.Terminals.LBRACK); }
"]"		{ return sym(Parser.Terminals.RBRACK); }
":"		{ return sym(Parser.Terminals.COLON); }
";"		{ return sym(Parser.Terminals.SEMI); }

"priorities" { return sym(Parser.Terminals.PRIORITIES); }
"left-associative" { return sym(Parser.Terminals.LEFTASSOCIATIVE); }
"right-associative" { return sym(Parser.Terminals.RIGHTASSOCIATIVE); }
"non-associative" { return sym(Parser.Terminals.NONASSOCIATIVE); }
"=="			{ return sym(Parser.Terminals.EQEQ); }

"("		{ return sym(Parser.Terminals.LPAREN); }
")"		{ return sym(Parser.Terminals.RPAREN); }
"sep"	{	yybegin(SEP);
			return sym(Parser.Terminals.SEP); }
"="		{	yybegin(REGEX);
			strbuf.setLength(0); }
{Identifier}	{ return sym(Parser.Terminals.ID, yytext()); }
\"		{	yybegin(STRING);
			strbuf.setLength(0); }
{AnyChar}	{ error("Unrecognized character: " + yytext()); }
}

<SEP> {
"="			{	yybegin(YYINITIAL); 
				return sym(Parser.Terminals.EQ); }
.			{	error("Expected = after sep, encountered " + yytext()); }
}

<REGEX> {
";"					{ yybegin(YYINITIAL);
						return sym(Parser.Terminals.REGEX, strbuf.toString()); }
"\\;"				{ strbuf.append(";"); }
"\\"				{ strbuf.append("\\"); }
//"\\\\"				{ strbuf.append('\\'); }
//\\.					{ error("Illegal escape character: " + yytext()); }
{RegexCharacter}*	{	strbuf.append(yytext()); }
{LineTerminator}	{ error("unterminated regular expression at end of line"); }
}

<STRING> {
\"					{ yybegin(YYINITIAL);
						return sym(Parser.Terminals.STRING_LITERAL, strbuf.toString()); }
"\\\""				{ strbuf.append("\\\""); }
"\\\\"				{ strbuf.append("\\\\"); }
\\.					{ error("Illegal escape character: " + yytext()); }
{StringCharacter}+	{ strbuf.append(yytext()); }
{LineTerminator}	{ error("unterminated string at end of line"); }
}
