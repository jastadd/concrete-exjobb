Concrete examples: PicoJavaExpressions
======================================

This is an implementation of PicoJava, extended with integer expressions, using Concrete. PicoJava must be present as a sibling directory to PicoJavaExpressions. PicoJavaExpressions is described in detail at http://jastadd.org/web/examples.php

To try the example from the command line:

    ant
    java -cp bin:<path to Beaver> PrettyPrint <input file>
    java -cp bin:<path to Beaver> PrintAST <input file>

There is an ANT target `test` that automatically runs the above programs on the provided test input files. There is also a target `clean` that removes all generated files. The target `gen` is intended for Eclipse users: it corresponds to the (default) `build` target without the `javac` step.

Directory structure
-------------------

* `spec/` Concrete grammars (`*.conc`) and JastAdd attribute grammars (`*.jrag`, `*.jadd`)
* `build.xml` ANT build script
* `README.md` This file
* `*.pj` Example input programs
* `bin/` (Generated directory) Java class files
* `gen/` (Generated directory) Generated specifications and Java source files
* `../picojava/` Must contain a `spec/` directory containing PicoJava grammars and a `src/` directory containing the Java source for the programs above

