Concrete examples: Integer expressions
======================================

This example demonstrates how to implement simple integer expressions with Concrete.

An example "program" in the specified language:

    1 + 1;
    2 * 3 + 3 - 2 / 2;
    0;
    4 - 5;
    -2 - (-4 + -5);

To try the example from the command line:

    ant
    java -cp bin:<path to Beaver> ASTPrinter <input file>
    java -cp bin:<path to Beaver> Evaluator <input file>

There is an ANT target `test` that automatically runs the above programs on the provided test input files. There is also a target `clean` that removes all generated files. The target `gen` is intended for Eclipse users: it corresponds to the (default) `build` target without the `javac` step.

Directory structure
-------------------

* `spec/` Concrete grammars (`*.conc`) and JastAdd attribute grammars (`*.jrag`, `*.jadd`)
* `src/` Java source files
* `build.xml` ANT build script
* `README.md` This file
* `testinput` Example input
* `bin/` (Generated directory) Java class files
* `gen/` (Generated directory) Generated specifications and Java source files
