import java.io.File;
import java.io.FileReader;

import ast.Start;

/**
 * A printer that outputs the integer value of each expression in the program to
 * System.out
 */
public class Evaluator {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Usage: java -cp <class path> Evaluator <filename>");
			System.exit(1);
		} else {
			try {
				Scanner scanner = new Scanner(new FileReader(new File(args[0])));
				Parser parser = new Parser();
				Start s = (Start) parser.parse(scanner);
				s.printValues();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}
