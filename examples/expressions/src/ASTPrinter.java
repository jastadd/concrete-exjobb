import java.io.File;
import java.io.FileReader;

import ast.Start;

/**
 * For printing the abstract syntax tree to System.out
 */
public class ASTPrinter {
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Usage: java -cp <class path> ASTPrinter <filename>");
			System.exit(1);
		} else {
			try {
				Scanner scanner = new Scanner(new FileReader(new File(args[0])));
				Parser parser = new Parser();
				Start s = (Start) parser.parse(scanner);
				s.printAST();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}
