import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import ast.*;

public class Test {
	public static void main(String[] args) throws IOException, Parser.Exception {
		Scanner scanner = new Scanner(new FileReader(new File(args[0], "testinput")));
		Parser parser = new Parser();
		try {
			Root r = (Root) parser.parse(scanner);
			r.printValue(System.out);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}
