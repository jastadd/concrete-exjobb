import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import AST.Program;

public class Test {

	public static void main(String[] args) throws IOException, Parser.Exception {
		Scanner scanner = new Scanner(new FileReader(new File(args[0], "legal.pj")));
		Parser parser = new Parser();
		Program p = (Program) parser.parse(scanner);
		java.util.Collection errors = p.errors();
		for (Object o : errors) {
			System.err.println(o);
		}
		
		scanner = new Scanner(new FileReader(new File(args[0], "illegal.pj")));
		p = (Program) parser.parse(scanner);
		errors = p.errors();
		for (Object o : errors) {
			System.err.println(o);
		}
	}

}
