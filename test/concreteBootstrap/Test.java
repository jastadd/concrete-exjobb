import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import ast.Start;
import ast.Grammar;
import main.Problem;

public class Test {
	public static void main(String[] args) throws IOException, Parser.Exception {
		Scanner scanner = new Scanner(new FileReader(new File(args[0], "concrete.conc")));
		Parser parser = new Parser();
		Start s = (Start) parser.parse(scanner);
		Grammar g = s.getGrammar();
		if (g.errors().isEmpty() && g.warnings().isEmpty()) {
			java.util.Properties props = new java.util.Properties();
			props.put("jflexpre", "jflextemplates/JFlexSpecPrelude.flex");
			props.put("jflexpost", "jflextemplates/JFlexSpecPostlude.flex");
			g.setOptions(props);
			PrintWriter out = new PrintWriter(System.out);
			g.dumpAstSpec(out);
			out.append('\n');
			g.writeJFlexSpec(out);
			out.append('\n');
			g.writeBeaverSpec(out);
			out.flush();
		} else {
			for (Problem p : g.errors()) {
				System.err.println(p);
			}
			for (Problem p : g.warnings()) {
				System.err.println(p);
			}
		}
	}
}
