model C1
 Real x;
end C1;

model C2
  extends C1(x=4);
  C1 c1(x=6);
end C2;

model C3

	extends C2(x=c1.x,c1(x=44));  
  
end C3;