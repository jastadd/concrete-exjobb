model C1 
  
  extends C2( redeclare replaceable C2.C33 c3a(v=12),x=2);
  
  model C2 
    extends C3(v=4);
   model C3 
     Real v;
     Real z;
   equation 
     v = 6;
     z = 7;
   end C3;
    
  model C33 
    Real v;
    Real z;
    Real x;
  equation 
    v=3;
    x=5;
  end C33;
   replaceable C3 c3a(z=B);
   replaceable C3 c3b;
   Real B=1;
   Real x=B;
  end C2;
  
  Real A=2;
  
  C2 c2a(redeclare C2.C33 c3a(v=1),v=5);
  C2 c2b(x=A,c3b(v=4));
  
end C1;
