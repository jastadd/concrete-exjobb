import java.io.FileReader;
import java.io.IOException;
import ast.*;
import java.io.File;
import java.io.FileNotFoundException;


public class Test {

	public static void main(String[] args) throws IOException, Parser.Exception {
		Scanner scanner = new Scanner(new FileReader(new File(args[0], "modifications8.mo")));
		Parser parser = new Parser();
		Root r = (Root) parser.parse(scanner);
		
		scanner = new Scanner(new FileReader(new File(args[0], "modifications10.mo")));
		r = (Root) parser.parse(scanner);
		
		scanner = new Scanner(new FileReader(new File(args[0], "redeclare0.mo")));
		r = (Root) parser.parse(scanner);
		
		scanner = new Scanner(new FileReader(new File(args[0], "redeclare1.mo")));
		r = (Root) parser.parse(scanner);
		
		scanner = new Scanner(new FileReader(new File(args[0], "extends_test.mo")));
		r = (Root) parser.parse(scanner);
		/*java.util.Collection errors = p.errors();
		for (Object o : errors) {
			System.err.println(o);
		}
		
		scanner = new Scanner(new FileReader(new File(args[0], "illegal.pj")));
		p = (Program) parser.parse(scanner);
		errors = p.errors();
		for (Object o : errors) {
			System.err.println(o);
		}*/
	}

}
