import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import ast.Grammar;
import ast.Start;

public class Test {
	public static void main(String[] args) throws IOException, Parser.Exception {
		Scanner scanner = new Scanner(new FileReader(new File(args[0], "testinput")));
		Parser parser = new Parser();
		Grammar g = ((Start) parser.parse(scanner)).getGrammar();
		g.pp();
	}
}
