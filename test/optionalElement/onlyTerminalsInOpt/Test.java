import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import ast.*;

public class Test {
	public static void main(String[] args) throws IOException {
		Root r = null;
		for (int i = 1; i <= 4; i++) {
			try {
				Scanner scanner = new Scanner(new FileReader(new File(args[0], "testinput" + i)));
				Parser parser = new Parser();
				r = (Root) parser.parse(scanner);
			} catch (beaver.Parser.Exception e) {
				System.err.println(e.getMessage());
			}
		}
		//r.getA();
		//r.getB();
	}
}