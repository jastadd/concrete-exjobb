import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import ast.Root;

public class Test {
	public static void main(String[] args) throws IOException, Parser.Exception {
		for (int i = 1; i <= 5; i++) {
			Scanner scanner = new Scanner(new FileReader(new File(args[0], "testinput" + i)));
			Parser parser = new Parser();
			Root r = (Root) parser.parse(scanner);
		}
	}
}