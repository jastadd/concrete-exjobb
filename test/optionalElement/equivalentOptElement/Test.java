import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import ast.*;

public class Test {
	public static void main(String[] args) throws IOException, Parser.Exception {
		for (int i = 1; i <= 3; i++) {
			Scanner scanner = new Scanner(new FileReader(new File(args[0], "testinput" + i)));
			Parser parser = new Parser();
			A r = (A) parser.parse(scanner);
		}
	}
}