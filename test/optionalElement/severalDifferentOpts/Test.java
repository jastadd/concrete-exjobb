import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import ast.*;

public class Test {
	public static void main(String[] args) throws IOException, Parser.Exception {
		Root r = null;
		for (int i = 1; i <= 3; i++) {
			Scanner scanner = new Scanner(new FileReader(new File(args[0], "testinput" + i)));
			Parser parser = new Parser();
			r = (Root) parser.parse(scanner);
		}
		r.hasA();
		r.getA();
		r.hasOptA();
		r.getOptA();
		r.hasB();
		r.getB();
	}
}