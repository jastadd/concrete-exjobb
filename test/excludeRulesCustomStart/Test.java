import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import ast.ExpProg;


public class Test {
	public static void main(String[] args) throws IOException, Parser.Exception {
		Scanner scanner = new Scanner(new FileReader(new File(args[0], "testinput")));
		Parser parser = new Parser();
		ExpProg r = (ExpProg) parser.parse(scanner);
	}
}